import { Router } from "express";
import * as homeController from "../controllers/home";
import * as userController from "../controllers/user";
import {IExpressRoute} from "./index";

/**
 * Root router to handle main app functionality
 * @param {string} path
 * @class {IExpressRoute} Root
 */
export class Root implements IExpressRoute {

  public router: Router;
  public path: string;

  public constructor(path: string) {
    this.router = Router();
    this.path = path;
    this.init();
  }

  private init() {
    this.router.get("/", homeController.index);
    this.router.get("/login", userController.getLogin);
    this.router.post("/login", userController.postLogin);
    this.router.get("/logout", userController.logout);
    this.router.get("/forgot", userController.getForgot);
    this.router.post("/forgot", userController.postForgot);
    this.router.get("/reset/:token", userController.getReset);
    this.router.post("/reset/:token", userController.postReset);
    this.router.get("/signup", userController.getSignup);
    this.router.post("/signup", userController.postSignup);
  }

}
