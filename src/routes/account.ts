import { Router } from "express";
import * as passportConfig from "../config/passport";
import * as userController from "../controllers/user";
import {IExpressRoute} from "./index";

/**
 * Account router to handle user data functionality
 * @param {string} path
 * @class {IExpressRoute} Root
 */
export class Account implements IExpressRoute {

  public router: Router;
  public path: string;

  public constructor(path: string) {
    this.router = Router();
    this.path = path;
    this.init();
  }
  private init() {
    this.router.get("/", passportConfig.isAuthenticated, userController.getAccount);
    this.router.post("/profile", passportConfig.isAuthenticated, userController.postUpdateProfile);
    this.router.post("/password", passportConfig.isAuthenticated, userController.postUpdatePassword);
    this.router.post("/delete", passportConfig.isAuthenticated, userController.postDeleteAccount);
    this.router.get("/unlink/:provider", passportConfig.isAuthenticated, userController.getOauthUnlink);
  }
}
