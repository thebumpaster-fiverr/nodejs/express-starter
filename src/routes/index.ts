import {Router} from "express";
import {Account} from "./account";
import {Api} from "./api";
import {Contact} from "./contact";
import {Oauth} from "./oauth";
import {Root} from "./root";

/**
 * Main interface to use for new route groups
 * @interface {router, path} IExpressRoute
 */
export interface IExpressRoute {
  router: Router;
  path: string;
}

/**
 * Main class to import routes into express application
 * @class {routes} expressRoutes
 */
export class ExpressRoutes {
  public routes: IExpressRoute[];

  public constructor() {
    this.init();
  }
  private init() {
    // Define routes
    this.routes = [
      new Root("/"),
      new Oauth("/auth"),
      // new Account("/account"),
      new Contact("/contact"),
      // new Api("/api"),
    ];

  }
}
