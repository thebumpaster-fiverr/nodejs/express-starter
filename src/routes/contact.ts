import { Router } from "express";
import * as contactController from "../controllers/contact";
import {IExpressRoute} from "./index";

/**
 * Contact router to handle contact app functionality
 * @param {string} path
 * @class {IExpressRoute} Root
 */
export class Contact implements IExpressRoute {

  public router: Router;
  public path: string;

  public constructor(path: string) {
    this.router = Router();
    this.path = path;
    this.init();
  }
  private init() {
    this.router.get("/", contactController.getContact);
    this.router.post("/", contactController.postContact);
  }
}
