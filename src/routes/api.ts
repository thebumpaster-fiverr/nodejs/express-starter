import { Router } from "express";
import * as passportConfig from "../config/passport";
import * as apiController from "../controllers/api";
import {IExpressRoute} from "./index";

/**
 * API router to handle third-party app functionality
 * @param {string} path
 * @class {IExpressRoute} Root
 */
export class Api implements IExpressRoute {

  public router: Router;
  public path: string;

  public constructor(path: string) {
    this.router = Router();
    this.path = path;
    this.init();
  }
  private init() {
    this.router.get("/", apiController.getApi);
    this.router.get("/facebook", passportConfig.isAuthenticated, apiController.getFacebook);
  }
}
