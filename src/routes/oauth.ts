import { Router } from "express";
import * as passport from "passport";
import {IExpressRoute} from "./index";

/**
 * Oauth router to handle app authorization for facebook
 * @param {string} path
 * @class {IExpressRoute} Root
 */
export class Oauth implements IExpressRoute {

  public router: Router;
  public path: string;

  public constructor(path: string) {
    this.router = Router();
    this.path = path;
    this.init();
  }
  private init() {
    this.router.get("/facebook", passport.authenticate("facebook", { scope: ["email", "public_profile"] }));
    this.router.get("/facebook/callback",
      passport.authenticate("facebook", { failureRedirect: "/login" }), (req, res) => {
        res.redirect(req.session.returnTo || "/");
      });
  }
}
