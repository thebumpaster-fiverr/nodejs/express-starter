/**
 * Application Module.
 */
import {App} from "./config/express";

/**
 * Export main express app
 */
export default new App().express;
