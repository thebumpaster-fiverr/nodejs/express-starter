import { MongoError } from "mongodb";
import * as mongoose from "mongoose";

/**
 * Main class to configure MongoDB Connection
 * @class {IMongoDBConfig} MongoDBConfig
 */
export class MongoDBConfig implements IMongoDBConfig {

  public static generateMongoURI(): { url: string, options: object } {

    let url = `mongodb://${process.env.MONGODB_HOST}:${process.env.MONGODB_PORT}/${process.env.MONGODB_NAME}`;
    const options = { autoReconnect: true, useNewUrlParser: true };

    if (process.env.MONGO_AUTHSOURCE) {
      url += `?authSource=${process.env.MONGO_AUTHSOURCE}&w=1`;
      Object.assign(options, {
        auth: {
          authdb: process.env.MONGO_AUTHSOURCE,
          password: process.env.MONGO_PASSWORD,
          user: process.env.MONGO_USERNAME,
        },
      });
    }

    if (process.env.MONGO_URI) {
      url = process.env.MONGO_URI;
    }

    return {url, options};
  }
  public status: boolean;
  private url: string;
  private options: object;

  constructor() {
    this.status = false;
    const {url, options} = MongoDBConfig.generateMongoURI();

    this.url = url;
    this.options = options;
  }

  public init(): void {

    mongoose.connect(this.url, this.options);

    mongoose.connection.on("connecting", () => {
      // tslint:disable-next-line:no-console
      console.log("MongoDB :: connecting");
      this.status = false;
    });

    mongoose.connection.on("error", (error: MongoError) => {
      // tslint:disable-next-line:no-console
      console.error(`MongoDB :: connection ${error}`, "MongoDB");
      this.status = false;
    });

    mongoose.connection.on("connected", () => {
      // tslint:disable-next-line:no-console
      console.log("MongoDB :: connected");
      this.status = true;
    });

    mongoose.connection.once("open", () => {
      // tslint:disable-next-line:no-console
      console.log("MongoDB :: connection opened");
      this.status = true;
    });

    mongoose.connection.on("reconnected", () => {
      // tslint:disable-next-line:no-console
      console.warn("MongoDB :: reconnected");
      this.status = true;
    });

    mongoose.connection.on("reconnectFailed", () => {
      // tslint:disable-next-line:no-console
      console.warn("MongoDB :: reconnectFailed");
      this.status = false;
    });

    mongoose.connection.on("disconnected", () => {
      // tslint:disable-next-line:no-console
      console.warn("MongoDB :: disconnected");
      this.status = false;
    });

    mongoose.connection.on("fullsetup", () => {
      // tslint:disable-next-line:no-console
      console.log("MongoDB :: reconnecting... %d");
      this.status = false;
    });
  }

}

/**
 * @interface IMongoDBConfig
 */
export interface IMongoDBConfig {
  status: boolean;
  init: () => void;
}
