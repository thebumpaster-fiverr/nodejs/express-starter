/**
 * Module dependencies.
 */
import * as bodyParser from "body-parser";
import * as compression from "compression";
import * as mongo from "connect-mongo";
import * as errorHandler from "errorhandler";
import * as express from "express";
import * as flash from "express-flash";
import * as session from "express-session";
import * as lusca from "lusca";
import * as logger from "morgan";
import * as passport from "passport";
import * as path from "path";
import expressValidator = require("express-validator");
import * as dotenv from "dotenv";
import {ExpressRoutes, IExpressRoute} from "../routes";
import {IMongoDBConfig, MongoDBConfig} from "./mongodb";

/**
 * Load environment variables from .env file, where API keys and passwords are configured.
 */
dotenv.config({ path: ".env" });

/**
 * API keys and Passport configuration.
 * @class {express} App
 */
export class App {

  // ref to Express instance
  public express: express.Application;
  private readonly MongoStore = mongo(session);
  private readonly definedRoutes: IExpressRoute[];
  private mongoDB: IMongoDBConfig;

  constructor() {
    this.express = express();
    this.middleware();
    this.definedRoutes = new ExpressRoutes().routes;
    this.routes();
    // Initiate Mongoose Client
    this.mongoDB = new MongoDBConfig();
    this.mongoDB.init();
    // Initiate main app
    this.init();

  }

  /**
   * Express Middleware
   */
  private middleware(): void {
    // Get MongoDB Credentials for mongo session
    const {url, options} = MongoDBConfig.generateMongoURI();

    this.express.set("port", process.env.PORT || 3000);
    this.express.set("views", path.join(__dirname, "../../views"));
    this.express.set("view engine", "pug");
    this.express.use(compression());
    this.express.use(logger("dev"));
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: true }));
    this.express.use(expressValidator());
    this.express.use(session({
      resave: true,
      saveUninitialized: true,
      secret: process.env.SESSION_SECRET,
      store: new this.MongoStore({
        autoReconnect: true,
        url,
      }),
    }));
    this.express.use(passport.initialize());
    this.express.use(passport.session());
    this.express.use(flash());
    this.express.use(lusca.xframe("SAMEORIGIN"));
    this.express.use(lusca.xssProtection(true));
    this.express.use((req, res, next) => {
      res.locals.user = req.user;
      next();
    });
    this.express.use((req, res, next) => {
      // After successful login, redirect back to the intended page
      if (!req.user &&
        req.path !== "/login" &&
        req.path !== "/signup" &&
        !req.path.match(/^\/auth/) &&
        !req.path.match(/\./)) {
        req.session.returnTo = req.path;
      } else if (req.user &&
        req.path === "/account") {
        req.session.returnTo = req.path;
      }
      next();
    });

    /**
     * Serve public files with express static
     */
    this.express.use(express.static(path.join(__dirname, "../public"), { maxAge: 31557600000 }));
  }

  /**
   * Express Route loader.
   */
  private routes(): void {
    for (const route of this.definedRoutes) {
      this.express.use(route.path, route.router);
    }
  }

  private init() {
    /**
     * Start Express server.
     */
    this.express.use(errorHandler());

    this.express.listen(this.express.get("port"), () => {
      // tslint:disable-next-line:no-console
      console.log(("  App is running at http://localhost:%d in %s mode"),
        this.express.get("port"),
        this.express.get("env"));
      // tslint:disable-next-line:no-console
      console.log("  Press CTRL-C to stop\n");
    });
  }
}
