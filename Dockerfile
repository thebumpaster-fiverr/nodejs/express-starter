FROM node:10-alpine

RUN mkdir -p /home/node/webapp ; \
    mkdir -p /home/node/webapp/node_modules

WORKDIR /home/node/webapp
ENV NODE_ENV production
ENV PORT 8081

COPY package.json /home/node/webapp/package.json
RUN npm install --quiet
RUN npm install -g node-sass --unsafe-perm=true

COPY . /home/node/webapp
COPY .env /home/node/webapp/.env


RUN npm rebuild node-sass
RUN node-sass src/public/css/main.scss dist/public/css/main.css
RUN npm run build-ts
RUN npm run copy-static-assets

RUN chown -R node:node /home/node/webapp/dist

USER node

CMD ["npm", "run", "serve"]

EXPOSE $PORT
